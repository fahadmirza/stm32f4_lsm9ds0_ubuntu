/**
  ******************************************************************************
  * @file      lsm9ds0.c 
  * @author    Fahad Mirza
  * @version   V1.0
  * @date      29-June-2016
  * @modified  30-June-2016
  * @brief     LSM9DS0 Driver
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "lsm9ds0.h"




/* Private variables ---------------------------------------------------------*/
I2C_InitTypeDef  I2C_InitStructure;




/* Function Declaration ------------------------------------------------------*/
static void I2C1_GPIO_Config(void);
static void I2C1_Init(void);
static uint8_t I2C1_Mem_Read(uint8_t devAddress, uint16_t MemAddress);
static void I2C1_Mem_Write(uint8_t devAddress, uint16_t MemAddress, uint8_t _data);


/* Function Definition -------------------------------------------------------*/
/**
  * @brief  Initialize I2C1 and configure LSM9DS0
  * @param  none
  * @retval true:  device available on the bus
  *	    false: not available
  */
bool lsm9dso_init(void)
{
  I2C1_GPIO_Config();
  I2C1_Init();
  
  if(lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_WHO_AM_I_XM) != LSM9DS0_XM_ID)
  {
    return false;
  }
  
  lsm9ds0_config();
  
  return true;
}





/**
  * @brief  Configures the GPIOs and enable clock for I2C.
  * @param  None
  * @retval None
  */
void I2C1_GPIO_Config(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
    
  /* RCC Configuration */

  /*I2C Peripheral clock enable */
  RCC_APB1PeriphClockCmd(I2C1_CLK, ENABLE);
  
  /*SDA GPIO clock enable */
  RCC_AHB1PeriphClockCmd(I2C1_SDA_GPIO_CLK, ENABLE);
  
  /*SCL GPIO clock enable */
  RCC_AHB1PeriphClockCmd(I2C1_SCL_GPIO_CLK, ENABLE);
  
  /* Reset I2Cx IP */
  RCC_APB1PeriphResetCmd(I2C1_CLK, ENABLE);
  
  /* Release reset signal of I2Cx IP */
  RCC_APB1PeriphResetCmd(I2C1_CLK, DISABLE);

  
  /* GPIO Configuration */
  /*Configure I2C SCL pin */
  GPIO_InitStructure.GPIO_Pin 	= I2C1_SCL_PIN;
  GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_Init(I2C1_SCL_GPIO_PORT, &GPIO_InitStructure);
  
  /*Configure I2C SDA pin */
  GPIO_InitStructure.GPIO_Pin = I2C1_SDA_PIN;
  GPIO_Init(I2C1_SDA_GPIO_PORT, &GPIO_InitStructure);
    
  /* Connect PXx to I2C_SCL */
  GPIO_PinAFConfig(I2C1_SCL_GPIO_PORT, I2C1_SCL_SOURCE, I2C1_SCL_AF);
  
  /* Connect PXx to I2C_SDA */
  GPIO_PinAFConfig(I2C1_SDA_GPIO_PORT, I2C1_SDA_SOURCE, I2C1_SDA_AF);
}





/**
  * @brief  Configures I2C1
  * @param  None
  * @retval None
  */
void I2C1_Init(void)
{
  /* I2C Struct Initialize */
  I2C_InitStructure.I2C_Mode		    = I2C_Mode_I2C;
  I2C_InitStructure.I2C_DutyCycle	    = I2C_DutyCycle_2;
  I2C_InitStructure.I2C_OwnAddress1	    = 0x00; 	// Not necessary
  I2C_InitStructure.I2C_Ack 		    = I2C_Ack_Enable;
  I2C_InitStructure.I2C_ClockSpeed	    = 100000;
  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  /* I2C1 Peripheral Enable */
  I2C_Cmd(I2C1, ENABLE);   
  /* I2C Initialize */
  I2C_Init(I2C1, &I2C_InitStructure);
}





/**
  * @brief  Configure LSM9DS0.
  * @param  none
  * @retval none
  */  
void lsm9ds0_config(void)
{
  // 95Hz rate, Enable Gyro, Enable axes
  lsm9ds0_Gyro_Mem_Write(LSM9DS0_REGISTER_CTRL_REG1_G, 0x0F);

  // 100Hz rate, cont. update, enable axes
  lsm9ds0_XMag_Mem_Write(LSM9DS0_REGISTER_CTRL_REG1_XM, 0x67);

  // Temp sen disabled, Mag hi res, 50Hz rate, no interrupt
  lsm9ds0_XMag_Mem_Write(LSM9DS0_REGISTER_CTRL_REG5_XM, 0x70);

  // No filter, cont. update
  lsm9ds0_XMag_Mem_Write(LSM9DS0_REGISTER_CTRL_REG7_XM, 0x00);
  
  // Acceleration 2G
  uint8_t reg = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_CTRL_REG2_XM);
  reg &= ~(0x38);//0b00111000
  reg |= LSM9DS0_ACCELRANGE_2G;
  lsm9ds0_XMag_Mem_Write(LSM9DS0_REGISTER_CTRL_REG2_XM, reg);
  
  // Magnet 2 Gauss
  reg = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_CTRL_REG6_XM);
  reg &= ~(0x60); //0b01100000
  reg |= LSM9DS0_MAGGAIN_2GAUSS;
  lsm9ds0_XMag_Mem_Write(LSM9DS0_REGISTER_CTRL_REG6_XM, reg);
  
  // Gyro 245 DPS
  reg = lsm9ds0_Gyro_Mem_Read(LSM9DS0_REGISTER_CTRL_REG4_G);
  reg &= ~(0x30); //0b00110000
  reg |= LSM9DS0_GYROSCALE_245DPS;
  lsm9ds0_Gyro_Mem_Write(LSM9DS0_REGISTER_CTRL_REG4_G, reg);
}







/**
  * @brief  Read single register from Accelerometer-Magnetometer
  * @param  MemAddress: Register Address
  * @retval Register value
  */
uint8_t lsm9ds0_XMag_Mem_Read(uint16_t MemAddress)
{
  return I2C1_Mem_Read(LSM9DS0_ADDRESS_XMAG, MemAddress);
}




/**
  * @brief  Read single register from Gyro
  * @param  MemAddress: Register Address
  * @retval Register value
  */
uint8_t lsm9ds0_Gyro_Mem_Read(uint16_t MemAddress)
{
  return I2C1_Mem_Read(LSM9DS0_ADDRESS_GYRO, MemAddress);
}





/**
  * @brief  Read single register using I2C
  * @param  devAddress: The 7bit address of the device/sensor
  *         MemAddress: Register Address
  * @retval Register value
  */
uint8_t I2C1_Mem_Read(uint8_t devAddress, uint16_t MemAddress)
{
  uint8_t temp_data_holder;
  /*!< While the bus is busy */
  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));	// Fixme: Timeout needed
  
  /*!< Send START condition */
  I2C_GenerateSTART(I2C1, ENABLE);
  
  /*!< Test on EV5 and clear it (cleared by reading SR1 then writing to DR) */
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));
 
  /*!< Send device address for write */
  I2C_Send7bitAddress(I2C1, devAddress, I2C_Direction_Transmitter);

  /*!< Test on EV6 and clear it */
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
  
  /*!< Send the device's internal address to read from: Only one byte address */
  I2C_SendData(I2C1, MemAddress);  

  /*!< Test on EV8 and clear it */
  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BTF) == RESET);
  
  /*!< Send STRAT condition a second time */  
  I2C_GenerateSTART(I2C1, ENABLE);
  
  /*!< Test on EV5 and clear it (cleared by reading SR1 and SR2) */
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT)); 
  
  /*!< Send device address for read */
  I2C_Send7bitAddress(I2C1, devAddress, I2C_Direction_Receiver); 

  /* Wait on ADDR flag to be set */
  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_ADDR) == RESET);
    
  /*!< Disable Acknowledgement */
  I2C_AcknowledgeConfig(I2C1, DISABLE);   
    
  /* Clear ADDR register by reading SR1 then SR2 register */
  (void)I2C1->SR1;
  (void)I2C1->SR2;
    
  /*!< Send STOP Condition */
  I2C_GenerateSTOP(I2C1, ENABLE);
    
  /* Wait for the byte to be received */
  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_RXNE) == RESET);

  /*!< Read the byte received from the device */
  temp_data_holder = I2C_ReceiveData(I2C1);      
    
  /* Wait to make sure that STOP control bit has been cleared */
  while(I2C1->CR1 & I2C_CR1_STOP); 
    
  /*!< Re-Enable Acknowledgement to be ready for another reception */
  I2C_AcknowledgeConfig(I2C1, ENABLE);

  return temp_data_holder;
}





/**
  * @brief  Write to single register of Accelerometer-Magnetometer.
  * @param  MemAddress: Register Address
  *         data      : byte to write
  * @retval none
  */
void lsm9ds0_XMag_Mem_Write(uint16_t MemAddress, uint8_t _data)
{
  I2C1_Mem_Write(LSM9DS0_ADDRESS_XMAG, MemAddress, _data);
}




/**
  * @brief  Write to single register of Gyro
  * @param  MemAddress: Register Address
  *         data      : byte to write
  * @retval none
  */
void lsm9ds0_Gyro_Mem_Write(uint16_t MemAddress, uint8_t _data)
{
  I2C1_Mem_Write(LSM9DS0_ADDRESS_GYRO, MemAddress, _data);
}








/**
  * @brief  Write to single register using I2C
  * @param  devAddress: The 7bit address of the device/sensor
  *         MemAddress: Register Address
  *         data      : 8bit valuef or the register
  * @retval none
  */
void I2C1_Mem_Write(uint8_t devAddress, uint16_t MemAddress, uint8_t _data)
{

  /*!< While the bus is busy */
  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));	// Fixme: Timeout needed
  
  /*!< Send START condition */
  I2C_GenerateSTART(I2C1, ENABLE);
  
  /*!< Test on EV5 and clear it (cleared by reading SR1 and SR2) */
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));
 
  /*!< Send device address for write */
  I2C_Send7bitAddress(I2C1, devAddress, I2C_Direction_Transmitter);

  /*!< Test on EV6 and clear it */
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
  
  /*!< Send the device's internal address to read from */
  I2C_SendData(I2C1, MemAddress);  

  /*!< Test on EV8 and clear it */
  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BTF) == RESET);
  
  /*!< Send data for internal address */
  I2C_SendData(I2C1, _data);   

  /*!< Test on EV8 and clear it */
  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BTF) == RESET);
    
  /*!< Send STOP Condition */
  I2C_GenerateSTOP(I2C1, ENABLE);

  /* Wait to make sure that STOP control bit has been cleared */
  while(I2C1->CR1 & I2C_CR1_STOP); 
}







/**
  * @brief  Read Accel data.
  * @param  accelData: lsm9ds0 Data Handle pointer
  * @retval None
  */
void lsm9ds0_Accel_Data_Read(lsm9ds0Vector_t *accelData)
{
  uint8_t buffer[6];
  
  buffer[0] = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_OUT_X_L_A);
  buffer[1] = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_OUT_X_H_A);
  buffer[2] = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_OUT_Y_L_A);
  buffer[3] = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_OUT_Y_H_A);
  buffer[4] = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_OUT_Z_L_A);
  buffer[5] = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_OUT_Z_H_A);

  // Shift values to create properly formed integer (low byte first)
  accelData->x = (buffer[1] << 8) | buffer[0];
  accelData->y = (buffer[3] << 8) | buffer[2];
  accelData->z = (buffer[5] << 8) | buffer[4];
}






/**
  * @brief  Read Mag Data.
  * @param  magData: lsm9ds0 Data Handle pointer
  * @retval None
  */
void lsm9ds0_Mag_Data_Read(lsm9ds0Vector_t *magData)
{
  uint8_t buffer[6];
  
  buffer[0] = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_OUT_X_L_M);
  buffer[1] = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_OUT_X_H_M);
  buffer[2] = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_OUT_Y_L_M);
  buffer[3] = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_OUT_Y_H_M);
  buffer[4] = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_OUT_Z_L_M);
  buffer[5] = lsm9ds0_XMag_Mem_Read(LSM9DS0_REGISTER_OUT_Z_H_M);
  
  // Shift values to create properly formed integer (low byte first)
  magData->x = (buffer[1] << 8) | buffer[0];
  magData->y = (buffer[3] << 8) | buffer[2];
  magData->z = (buffer[5] << 8) | buffer[4];
}






/**
  * @brief  Read gyroscope Data.
  * @param  gyroData: lsm9ds0 Data Handle pointer
  * @retval None
  */
void lsm9ds0_Gyro_Data_Read(lsm9ds0Vector_t *gyroData)
{
  uint8_t buffer[6];

  buffer[0] = lsm9ds0_Gyro_Mem_Read(LSM9DS0_REGISTER_OUT_X_L_G);
  buffer[1] = lsm9ds0_Gyro_Mem_Read(LSM9DS0_REGISTER_OUT_X_H_G);
  buffer[2] = lsm9ds0_Gyro_Mem_Read(LSM9DS0_REGISTER_OUT_Y_L_G);
  buffer[3] = lsm9ds0_Gyro_Mem_Read(LSM9DS0_REGISTER_OUT_Y_H_G);
  buffer[4] = lsm9ds0_Gyro_Mem_Read(LSM9DS0_REGISTER_OUT_Z_L_G);
  buffer[5] = lsm9ds0_Gyro_Mem_Read(LSM9DS0_REGISTER_OUT_Z_H_G);
  
  // Shift values to create properly formed integer (low byte first)
  gyroData->x = (buffer[1] << 8) | buffer[0];
  gyroData->y = (buffer[3] << 8) | buffer[2];
  gyroData->z = (buffer[5] << 8) | buffer[4];
}

