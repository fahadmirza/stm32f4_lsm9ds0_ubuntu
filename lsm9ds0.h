/**
  ******************************************************************************
  * @file      lsm9ds0.h 
  * @author    Fahad Mirza
  * @version   V1.1
  * @date      09-July-2016
  * @modified  09-July-2016
  * @brief     Header file of LSM9DS0 Driver
  ******************************************************************************
  */

#ifndef __LSM9DS0_H__
#define __LSM9DS0_H__


#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4_discovery.h"
#include "stdbool.h"


/************************************* I2C ************************************/
/* Private define ------------------------------------------------------------*/
#define I2C1_CLK                      RCC_APB1Periph_I2C1
#define I2C1_SDA_GPIO_CLK             RCC_AHB1Periph_GPIOB
#define I2C1_SDA_PIN                  GPIO_Pin_9                
#define I2C1_SDA_GPIO_PORT            GPIOB                       
#define I2C1_SDA_SOURCE               GPIO_PinSource9
#define I2C1_SDA_AF                   GPIO_AF_I2C1
  
#define I2C1_SCL_GPIO_CLK             RCC_AHB1Periph_GPIOB
#define I2C1_SCL_PIN                  GPIO_Pin_6                
#define I2C1_SCL_GPIO_PORT            GPIOB                    
#define I2C1_SCL_SOURCE               GPIO_PinSource6
#define I2C1_SCL_AF                   GPIO_AF_I2C1



/*********************************** LSM9DS0 **********************************/
/* Private define ------------------------------------------------------------*/
#define LSM9DS0_ADDRESS_XMAG		(0x3A) 
#define LSM9DS0_ADDRESS_GYRO		(0xD6)
#define LSM9DS0_XM_ID			(0x49)
#define LSM9DS0_G_ID			(0xD4)


// Linear Acceleration: mg per LSB
#define LSM9DS0_ACCEL_MG_LSB_2G 	(0.061F)
#define LSM9DS0_ACCEL_MG_LSB_4G 	(0.122F)
#define LSM9DS0_ACCEL_MG_LSB_6G 	(0.183F)
#define LSM9DS0_ACCEL_MG_LSB_8G 	(0.244F)
#define LSM9DS0_ACCEL_MG_LSB_16G	(0.732F)

// Magnetic Field Strength: gauss range
#define LSM9DS0_MAG_MGAUSS_2GAUSS      	(0.08F)
#define LSM9DS0_MAG_MGAUSS_4GAUSS      	(0.16F)
#define LSM9DS0_MAG_MGAUSS_8GAUSS      	(0.32F)
#define LSM9DS0_MAG_MGAUSS_12GAUSS     	(0.48F)

// Angular Rate: dps per LSB
#define LSM9DS0_GYRO_DPS_DIGIT_245DPS  	(0.00875F)
#define LSM9DS0_GYRO_DPS_DIGIT_500DPS  	(0.01750F)
#define LSM9DS0_GYRO_DPS_DIGIT_2000DPS 	(0.07000F)


/* Private typedef -----------------------------------------------------------*/ 
typedef enum
{
  LSM9DS0_REGISTER_WHO_AM_I_G          = 0x0F,
  LSM9DS0_REGISTER_CTRL_REG1_G         = 0x20,
  LSM9DS0_REGISTER_CTRL_REG3_G         = 0x22,
  LSM9DS0_REGISTER_CTRL_REG4_G         = 0x23,
  LSM9DS0_REGISTER_OUT_X_L_G           = 0x28,
  LSM9DS0_REGISTER_OUT_X_H_G           = 0x29,
  LSM9DS0_REGISTER_OUT_Y_L_G           = 0x2A,
  LSM9DS0_REGISTER_OUT_Y_H_G           = 0x2B,
  LSM9DS0_REGISTER_OUT_Z_L_G           = 0x2C,
  LSM9DS0_REGISTER_OUT_Z_H_G           = 0x2D,
} lsm9ds0GyroRegisters_t;
  
typedef enum
{
  LSM9DS0_REGISTER_TEMP_OUT_L_XM       = 0x05,
  LSM9DS0_REGISTER_TEMP_OUT_H_XM       = 0x06,
  LSM9DS0_REGISTER_STATUS_REG_M        = 0x07,
  LSM9DS0_REGISTER_OUT_X_L_M           = 0x08,
  LSM9DS0_REGISTER_OUT_X_H_M           = 0x09,
  LSM9DS0_REGISTER_OUT_Y_L_M           = 0x0A,
  LSM9DS0_REGISTER_OUT_Y_H_M           = 0x0B,
  LSM9DS0_REGISTER_OUT_Z_L_M           = 0x0C,
  LSM9DS0_REGISTER_OUT_Z_H_M           = 0x0D,
  LSM9DS0_REGISTER_WHO_AM_I_XM         = 0x0F,
  LSM9DS0_REGISTER_INT_CTRL_REG_M      = 0x12,
  LSM9DS0_REGISTER_INT_SRC_REG_M       = 0x13,
  LSM9DS0_REGISTER_CTRL_REG1_XM        = 0x20,
  LSM9DS0_REGISTER_CTRL_REG2_XM        = 0x21,
  LSM9DS0_REGISTER_CTRL_REG5_XM        = 0x24,
  LSM9DS0_REGISTER_CTRL_REG6_XM        = 0x25,
  LSM9DS0_REGISTER_CTRL_REG7_XM        = 0x26,
  LSM9DS0_REGISTER_OUT_X_L_A           = 0x28,
  LSM9DS0_REGISTER_OUT_X_H_A           = 0x29,
  LSM9DS0_REGISTER_OUT_Y_L_A           = 0x2A,
  LSM9DS0_REGISTER_OUT_Y_H_A           = 0x2B,
  LSM9DS0_REGISTER_OUT_Z_L_A           = 0x2C,
  LSM9DS0_REGISTER_OUT_Z_H_A           = 0x2D,
} lsm9ds0MagAccelRegisters_t;

typedef enum
{
  LSM9DS0_ACCELRANGE_2G                = (0x00<<3),
  LSM9DS0_ACCELRANGE_4G                = (0x01<<3),
  LSM9DS0_ACCELRANGE_6G                = (0x02<<3),
  LSM9DS0_ACCELRANGE_8G                = (0x03<<3),
  LSM9DS0_ACCELRANGE_16G               = (0x04<<3)
} lsm9ds0AccelRange_t;

typedef enum
{
  LSM9DS0_MAGGAIN_2GAUSS               = (0x00<<5),  // +/- 2 gauss
  LSM9DS0_MAGGAIN_4GAUSS               = (0x01<<5),  // +/- 4 gauss
  LSM9DS0_MAGGAIN_8GAUSS               = (0x02<<5),  // +/- 8 gauss
  LSM9DS0_MAGGAIN_12GAUSS              = (0x03<<5)   // +/- 12 gauss
} lsm9ds0MagGain_t;


typedef enum
{
  LSM9DS0_GYROSCALE_245DPS             = (0x00<<4),  // +/- 245 degrees per second rotation
  LSM9DS0_GYROSCALE_500DPS             = (0x01<<4),  
  LSM9DS0_GYROSCALE_2000DPS            = (0x02<<4)   
} lsm9ds0GyroScale_t;


typedef struct vector_s
{
  int16_t x;
  int16_t y;
  int16_t z;
} lsm9ds0Vector_t;



/* Function Definitions ------------------------------------------------------*/
bool lsm9dso_init(void);
void lsm9ds0_config(void);

uint8_t lsm9ds0_XMag_Mem_Read(uint16_t MemAddress);
uint8_t lsm9ds0_Gyro_Mem_Read(uint16_t MemAddress);
void lsm9ds0_XMag_Mem_Write(uint16_t MemAddress, uint8_t data);
void lsm9ds0_Gyro_Mem_Write(uint16_t MemAddress, uint8_t data);

void lsm9ds0_Accel_Data_Read(lsm9ds0Vector_t *accelData);
void lsm9ds0_Mag_Data_Read(lsm9ds0Vector_t *magData);
void lsm9ds0_Gyro_Data_Read(lsm9ds0Vector_t *gyroData);

#ifdef __cplusplus
}
#endif


#endif
