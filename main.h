/**
  ******************************************************************************
  * @file    main.h 
  * @author  Fahad Mirza
  * @version V1.1
  * @date    09-July-2016
  * @brief   Header for main.c
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H


/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_conf.h"
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "my_printf.h"
#include "lsm9ds0.h"




/* Private define ------------------------------------------------------------*/
//#define STREAM_MODE
#define FREEFALL_MODE
#define FREEFALL_THRESHOLD 7000


/* Private variables ---------------------------------------------------------*/
__IO uint32_t g_timing_delay;
__IO uint32_t g_ticks = 0; // increments every millisecond

// Constant Strings
const char WelcomeInstruction[] = "Press the Blue button to start...\r\n";
const char Error[] 		= "Error!!!";
const char Ready[] 		= "Ready.\r\n";
const char DevNotFound[] 	= "Device is not available.\r\n";
const char Freefall[] 		= "Freefall!!!!\r\n";

/* Private function prototypes -----------------------------------------------*/
void timing_delay_decrement(void);
void delay_ms(uint32_t t);
void init_UART4();
void init_LED();
void init_blue_push_button();
uint32_t get_ticks();
void Sensor_Data_Display(const lsm9ds0Vector_t *accelData, const lsm9ds0Vector_t *magData, const lsm9ds0Vector_t *gyroData);
uint16_t intToStr(int16_t num, char str[]);
void reverse(char *str, uint16_t len);
int16_t absolute(int16_t num);



/* Function Definitions -----------------------------------------------------*/
/**
  * @brief  Raw LSM9DS0 sensor data display
  * @param  accelData,magData,gyroData: lsm9ds0 Data Handle pointer
  * @retval None
  */
void Sensor_Data_Display(const lsm9ds0Vector_t *accelData, const lsm9ds0Vector_t *magData, const lsm9ds0Vector_t *gyroData)
{
  char str[10];
  
  my_printf("\r\nACC:\tX="); intToStr(accelData->x, str); my_printf(str);
  my_printf("\tY="); intToStr(accelData->y, str); my_printf(str);                
  my_printf("\tZ="); intToStr(accelData->z, str); my_printf(str);
  
  my_printf("\r\nMAG:\tX="); intToStr(magData->x, str); my_printf(str);
  my_printf("\tY="); intToStr(magData->y, str); my_printf(str);                
  my_printf("\tZ="); intToStr(magData->z, str); my_printf(str);
  
  my_printf("\r\nGYRO:\tX="); intToStr(gyroData->x, str); my_printf(str);
  my_printf("\tY="); intToStr(gyroData->y, str); my_printf(str);                
  my_printf("\tZ="); intToStr(gyroData->z, str); my_printf(str);
  my_printf("\r\n");
}





/**
  * @brief  int (signed) to string converter (-32768 to 32768)
  * @param  num: integer number    
  *         str: buffer pointer
  * @retval Length of buffer
  */
uint16_t intToStr(int16_t num, char str[])
{
    uint16_t i = 0;
    bool negativeNum = false;
    if(num<0)
    {
      str[i++] = '-';
      negativeNum = true;
      num = -num;
    }
    while (num)
    {
        str[i++] = (num%10) + '0';
        num = num/10;
    }
    
    if(negativeNum)
      reverse(str+1, i-1);
    else
      reverse(str, i);
    str[i] = '\0';
    return i;
}

/**
  * @brief  Reverses a string 'str' of length 'len'
  * @param  str: buffer pointer   
  *         len: buffer length
  * @retval None
  */
void reverse(char *str, uint16_t len)
{
    uint16_t i=0, j=len-1; 
    uint8_t temp;
    while (i<j)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++; j--;
    }
}


/**
  * @brief  Provides absolute value of a signed integer
  * @param  num: Signed integer
  * @retval Absolute value
  */
int16_t absolute(int16_t num)
{
  return ((num<0) ? -num : num);
}


#endif /* __MAIN_H */
